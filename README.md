# SMB to FTP for Backups
Testé avec python 2.7 et 3.4 sur un Raspberry Pi 2 Modèle B.

Ce script a été écrit pour être utilisé sur un Raspberry Pi.  

## Table des matières
- [Pre-requis](#prerequis)
- [Installation](#installation)
- [Configuration](#configuration)


## Pre-requis
- Un Raspberry Pi
- Une carte (micro)SD avec l'image d'un Raspbian
- Une méthode pour accéder au terminal
    + Via un clavier + écran
    + Via SSH (`ssh pi@raspberry`, mot de passe par défaut: `raspberry`)


## Installation
- Mettez vos paquets à jours avant de commencer : `sudo apt-get update && sudo apt-get upgrade -y`
- Installez un client FTP et le nécessaire pour monter un partage Samba : `sudo apt-get install -y ftp cifs-utils`
- Transférez les fichiers `backup.py` et `configbkup.py.dist` dans un dossier sur le Raspberry Pi (via FTP ou SCP/SFTP)
- Faites un copie du fichier de configuration d’exemple `cp configbkup.py.dist configbkup.py`
- Et éditez-le `configbkup.py` (voir [Configuration](#configuration))
    + Avec nano: `nano configbkup.py`
    + Avec vi: `vi configbkup.py`
- Vous pouvez maintenant exécuter le programme en super-utilisateur `sudo python backup.py`

Pour programmer un lancement régulier, vous pouvez créer une tâche dans crontab avec l'utilisateur root (`sudo crontab -e -u root`):
```bash
@daily /usr/bin/python /home/pi/smbbackup/backup.py > /home/pi/smbbackup/cron.log 2&>1
```
> **Note** : Modifiez `/home/pi/smbbackup` vers le chemin absolu du script.  
> **IMPORTANT**: Si vous voulez démonter ou retirer le dossier "mount-backup", faites bien un `umount mount-backup` avant de supprimer le dossier ! Vous risquez de supprimer le contenu du partage.


## Configuration
#### `smb` - _Source, à sauvegarder_
```python
smb = {
        'host': '192.168.0.42',
        'share': 'partage/af',
        'user': 'af',
        'pass': 'af'
}
```
- `host`: L'IP ou le _hostname_ du partage à accéder
- `share`: Le nom du partage. Vous pouvez directement spécifier un sous-dossier (le séparateur de dossier est au format UNIX, `/`)
- `user`: Nom d'utilisateur pour accéder au partage
- `pass`: Et son mot de passe

-----

#### `ftp` - _Destination, stockage des sauvegardes_
```python
ftp = {
        'host': 'backup.example.com',
        'port': '21',
        'user': 'af',
        'pass': 'af',
}
```
- `host`: L'IP ou le _hostname_ du serveur FTP
- `port`: Port du FTP. Le port standard et 21
- `user`: Nom d'utilisateur pour accéder au FTP
- `pass`: Et son mot de passe

-----

#### `destination` - _Le chemin et fichier de destination_
```python
import datetime
currdatetime = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
destination = 'backups/' + currdatetime + '_backup.tar.gz'
```
La variable `destination` peut être un chemin relatif (par rapport au _home_ lors de l'accès en FTP) ou absolu.  
Dans l'exemple, une variable `currdatetime` est crée contenant la date au format `YYYY-mm-dd_HH-MM-SS` et est appliquée dans le nom du fichier.  

> **Note**: Le fichier **doit** avoir comme extension soit `.tar.gz`, soit `.tgz`

-----

#### `tobackup` - _Les fichiers et dossier à sauvegarder_
```python
tobackup = [
    "Je vais être sauvegardé!.txt",
    "dossier/sous-dossier/",
    "dossier à sauvegarder/"
]
```
Dans les accolades, ajoutez les fichiers et dossiers que vous voulez sauvegarder.  
Il s'agit d'une liste (_array_), contenez les chemin relatifs (par rapport au `share` défini plus haut) entre guillemets, séparés par des virgules.

Comme montré dans l'exemple, les caractères spéciaux sont autorisés (tant qu'ils sont permis par Windows) **à l'exception de l'apostrophe** (`'`).  
Le problème de l'apostrophe ne s'applique pas aux fichiers eux-mêmes, mais uniquement à cette liste dans la configuration.  
Vous pouvez spécifier directement les fichiers à sauvegarder ou des (sous-)dossiers (le contenu sera sauvegardé, récursivement).