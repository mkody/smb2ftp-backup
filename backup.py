# -*- encoding: utf-8 -*-
import os
import sys
import errno
import pipes
import subprocess
from configbkup import *

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

def umountshare():
	# On démonte le dossier s'il est déjà monté (pour éviter les problèmes)
	print('> Vérification que rien n\'est monté')
	subprocess.call('umount "' + THIS_DIR + '/mount-backup/"', shell=True)

def openshare():
	try: # Créer le dossier où l'on va monter le partage samba
		os.makedirs(THIS_DIR + '/mount-backup')
		print('> Le dossier "mount-backup" a été crée')
	except OSError as exception: # Une erreur peut être lancée...
		# Et si c'est pas parce que le dossier existe déjà, on stoppe
		if exception.errno != errno.EEXIST: 
			raise

	# On monte le partage. S'il y a une erreur, le script s'arrête
	umountshare() # On évite les problèmes...
	print('> Accès au partage SMB...')
	subprocess.check_call('mount -t cifs -o user="' + smb['user'] + '" -o password="' + smb['pass'] + '" -o sec=ntlm "//' + smb['host'] + '/' + smb['share'] + '" "' + THIS_DIR + '/mount-backup/"', shell=True)
	print('> Montage effectué')

def sendtoftp():
	print('> Démarrage de la sauvegarde')
	# On prends le chemin absolu des fichiers + on l'échape pour l'utiliser dans shell
	for i, t in enumerate(tobackup):
		tobackup[i] = pipes.quote(THIS_DIR + '/mount-backup/' + t)
	files = " ".join(tobackup)

	# Construction de la commande FTP
	command = "ftp -p -i -n %s %s << END_SCRIPT\n" % (ftp['host'], ftp['port'])
	command = "%s quote USER %s\n" % (command, ftp['user'])
	command = "%s quote PASS %s\n" % (command, ftp['pass'])
	command = "%s bin\n" % command
	command = "%s put \"| tar cvf - %s | gzip \" %s\n" % (command, files, destination)
	command = "%s quit\n" % command
	command = "%sEND_SCRIPT" % command

	# On execute la commande
	print('> Compression et envoi de l\'archive')
	subprocess.call(command, shell=True)

	print('> Tâche terminée')

if __name__ == '__main__':
	# On vérifie qu'on soit root ou sudo
	if os.geteuid() != 0: # Pas super portable
		print('>> Veuillez exécuter ce script avec la commande sudo ou en utilisateur root !')
		exit(1)

	openshare() # Monter le share
	sendtoftp() # Faire le transfert
	umountshare() # On en a plus besoin
	exit(0)
